
public class DecimalToRomanNumeral {

    // Find the roman numeral value of a given decimal for conversion.
    public String decimalToRoman(int num) {

        // Used String array making it easier to convert integer to roman numeral.
        // Added 'CM', 'XC', and 'IX' for converting numbers which are unknown to roman numerals.
        // Making it easier to compute roman numerals to string.
        String[] romanCharacters = { "M", "CM", "D", "C", "XC", "L", "X", "IX", "V", "I" };
        int[] decimalValues = { 1000, 900, 500, 100, 90, 50, 10, 9, 5, 1 };
        // Utilized StringBuilder for performance, than conventional string concatenation. i.e. += or +
        StringBuilder result = new StringBuilder();

        // Loops through the decimalValues array to find the match of each expected integer.
        for (int i = 0; i < decimalValues.length; i++) {

            // Finds what the whole number number when divided by the spot in place.
            int numberInPlace = num / decimalValues[i];

            // If the numberInPlace is a zero, then it starts from the beginning with a different number in "i".
            // Goes through the decimalValues array until it hits a value that isn't zero.
            if (numberInPlace == 0)
                continue;

            // It appends the result at the specific spot in romanCharacters.
            // E.g. if the spot is at romanCharacters[6], then it would append "L" on the end of the string.
            result.append(numberInPlace == 4 && i > 0 ? romanCharacters[i] + romanCharacters[i - 1] :
                    new String(new char[numberInPlace]).replace("\0", romanCharacters[i]));

            // Lastly, This will get the next number in romanCharacters sequence.
            num = num % decimalValues[i];
        }
        return result.toString();
    }
}
