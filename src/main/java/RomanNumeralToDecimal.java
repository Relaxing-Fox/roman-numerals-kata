
class RomanNumeralToDecimal {

    // This function returns value of a Roman symbol.
    private int romanToDecimalValue(char r) {

        // Used switch statement for shorter amount of code (6 lines of code).
        // Instead of using if statements which would take 13 lines of code.
        // Making it easier to convert char to integer.
        switch(r) {
            case 'I': return 1;
            case 'V': return 5;
            case 'X': return 10;
            case 'L': return 50;
            case 'C': return 100;
            case 'D': return 500;
            case 'M': return 1000;
        }
        return -1;
    }

    // Find the decimal value of a given roman numeral for conversion.
    public int romanToDecimal(String str) {
        // Initialize result.
        int result = 0;

        for (int i = 0; i < str.length(); i++) {
            // Getting value of symbol string1[i].
            int string1 = romanToDecimalValue(str.charAt(i));

            if (i+1 < str.length()) {
                // Getting value of symbol string2[i+1].
                int string2 = romanToDecimalValue(str.charAt(i+1));

                // Comparing both values.
                if (string1 >= string2) {
                    // Value of current symbol is greater or equal to the next symbol.
                    result += string1;
                } else {
                    result += string2 - string1;
                    i++; // Value of current symbol is less than the next symbol
                }
            } else {
                result += string1;
                i++;
            }
        }

        return result;
    }
}
