import org.junit.Before;
import org.junit.Test;
import org.junit.Assert;

public class RomanNumeralToDecimalTest {

    public RomanNumeralToDecimal romanNumeral;

    @Before
    public void setUp() throws Exception {
        romanNumeral = new RomanNumeralToDecimal();
    }

    @Test
    public void testRomanNumeralToDecimal_1() {
        Assert.assertEquals(romanNumeral.romanToDecimal("I"), 1);
    }

    @Test
    public void testRomanNumeralToDecimal_3() {
        Assert.assertEquals(romanNumeral.romanToDecimal("III"), 3);
    }

    @Test
    public void testRomanNumeralToDecimal_9() {
        Assert.assertEquals(romanNumeral.romanToDecimal("IX"), 9);
    }

    @Test
    public void testRomanNumeralToDecimal_1066() {
        Assert.assertEquals(romanNumeral.romanToDecimal("MLXVI"), 1066);
    }

    @Test
    public void testRomanNumeralToDecimal_1989() {
        Assert.assertEquals(romanNumeral.romanToDecimal("MCMLXXXIX"), 1989);
    }
}
