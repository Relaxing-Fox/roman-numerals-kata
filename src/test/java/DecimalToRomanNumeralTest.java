import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DecimalToRomanNumeralTest {

    public DecimalToRomanNumeral decimalToRoman;

    @Before
    public void setUp() throws Exception {
        decimalToRoman = new DecimalToRomanNumeral();
    }

    @Test
    public void testDecimalToRomanNumeral_I() {
        Assert.assertEquals(decimalToRoman.decimalToRoman(1), "I");
    }

    @Test
    public void testDecimalToRomanNumeral_III() {
        Assert.assertEquals(decimalToRoman.decimalToRoman(3), "III");
    }

    @Test
    public void testDecimalToRomanNumeral_IX() {
        Assert.assertEquals(decimalToRoman.decimalToRoman(9), "IX");
    }

    @Test
    public void testDecimalToRomanNumeral_MLXVI() {
        Assert.assertEquals(decimalToRoman.decimalToRoman(1066), "MLXVI");
    }

    @Test
    public void testDecimalToRomanNumeral_MCMLXXXIX() {
        Assert.assertEquals(decimalToRoman.decimalToRoman(1989), "MCMLXXXIX");
    }
}
